import pokemonApi from "@/api/pokemonApi"

const getPokemons = () => {
    const pokemonArray = Array.from( Array(650) )

    return pokemonArray.map( (_, index) => index + 1 )
}

const getPokemonOptions = async () => {

    const mixPokemon =  getPokemons()
                        .sort( () => Math.random() - 0.5)

    const pokemons = await getPokemonNames( mixPokemon.splice(0,4) ); 

    console.table(pokemons);

    return pokemons
}

const getPokemonNames = async ([pokemonID_1,pokemonID_2,pokemonID_3,pokemonID_4]) => {

    const [pokemon1, pokemon2, pokemon3, pokemon4] = await Promise.all([
        pokemonApi.get(`/${pokemonID_1}`).then(({ data }) => data),
        pokemonApi.get(`/${pokemonID_2}`).then(({ data }) => data),
        pokemonApi.get(`/${pokemonID_3}`).then(({ data }) => data),
        pokemonApi.get(`/${pokemonID_4}`).then(({ data }) => data),
    ])
    
    return [
        { id: pokemon1.id, name: pokemon1.name},
        { id: pokemon2.id, name: pokemon2.name},
        { id: pokemon3.id, name: pokemon3.name},
        { id: pokemon4.id, name: pokemon4.name}
    ]

}

export default getPokemonOptions